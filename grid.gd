extends Node2D

var grid: Array
var cell = preload("res://cell.tscn")
@export var size: int = 71
@export var speed: float = 0.5

class Strategy:
	static var funcs = {}
	static var min_live = 3
	static var max_live = 3
	static var min_die = 2
	static var max_die = 3
	static func death_to_life(count):
		return count >= min_live and count <= max_live

	static func life_to_death(count):
		return count < min_die or count > max_die

	static func toggle(e):
		return funcs["toggle"].call(e)

	static func edge(p, d):
		return funcs["edge"].call(p, d)

func _ready():
	Strategy.funcs["toggle"] = on_mouse_click
	Strategy.funcs["edge"] = wrap_grid
	build_grid()

func build_grid():
	get_tree().call_group("cell", "queue_free")
	grid = []
	var x = 0
	var y = 100
	grid.resize(size)
	
	for i in range(size):
		var row = []
		row.resize(size)
		grid[i] = row
		for j in range(size):
			var c = cell.instantiate().new(i, j, Strategy)
			row[j] = c
			add_child(c)

func reset():
	get_tree().call_group("cell", "reset")

func tick():
	get_tree().call_group("cell", "calculate_generation")
	get_tree().call_group("cell", "tick_generation")
	
func start():
	%Timer.start()

func stop():
	%Timer.stop()

func _change_speed(speed):
	%Timer.wait_time = speed

func on_mouse_click(event):
	return event is InputEventMouseButton and event.pressed

func on_mouse_move(event):
	return event is InputEventMouseMotion

func wrap_grid(pos, diff):
	return Vector2(int(pos.x + diff.x) % size, int(pos.y + diff.y) % size)

func block_grid(pos, diff):
	var change = pos + diff
	if change.x < 0 or change.y < 0 or change.x >= size or change.y >= size:
		return Vector2.ZERO
	return change

func _on_change_rules():
	if %Rules.rules["toggle"] == "click":
		Strategy.funcs["toggle"] = on_mouse_click
	elif %Rules.rules["toggle"] == "move":
		Strategy.funcs["toggle"] = on_mouse_move
	
	if %Rules.rules["edge"] == "wrap":
		Strategy.funcs["edge"] = wrap_grid
	elif %Rules.rules["edge"] == "block":
		Strategy.funcs["edge"] = block_grid


func _alive_less_rule(value):
	Strategy.min_live = value

func _alive_more_rule(value):
	Strategy.max_live = value

func _dead_less_rule(value):
	Strategy.min_die = value

func _dead_more_rule(value):
	Strategy.max_die = value
