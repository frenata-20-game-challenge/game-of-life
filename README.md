# Conway's Game of Life

https://20_games_challenge.gitlab.io/challenge/

## Features

 * greedy computation of 'alive'ness
 * wraparound map
 * runtime-adjustable rules

## TODO

 * larger map with a moveable viewport
 * expandable map
 * import standardized format of inputs

