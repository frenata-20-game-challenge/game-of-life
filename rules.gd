extends Control

var rules = {"toggle": "click", "edge": "wrap"}

func _on_change_rules():
	visible = false
	%Game.visible = true

func _on_enter_rules():
	visible = true
	%Game.visible = false

func _on_toggle_toggled(button_pressed):
	rules["toggle"] = "move" if button_pressed else "click"
	if button_pressed:
		%ToggleRule/Value.text = "Move"
	else:
		%ToggleRule/Value.text = "Click"

func _on_edge_toggled(button_pressed):
	rules["edge"] = "block" if button_pressed else "wrap"
	if button_pressed:
		%EdgeRule/Value.text = "Block"
	else:
		%EdgeRule/Value.text = "Wrap"


func _on_reset_rules():
	%EdgeRule/Value.button_pressed = false
	%ToggleRule/Value.button_pressed = false
	%LiveLessRule/Value.value = 3
	%LiveMoreRule/Value.value = 3
	%DeathLessRule/Value.value = 2
	%DeathMoreRule/Value.value = 3
