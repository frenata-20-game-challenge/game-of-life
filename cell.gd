extends Node2D

@export var x_offset = 0
@export var y_offset = 100

var size = 5
var padding = 2
var state = 0
var next = 0
var pos: Vector2

var now_count = 0
var next_count = 0

var strategy

const NEIGHBORS = [Vector2(-1,-1), Vector2(-1,0), Vector2(-1,1), Vector2(0,1), Vector2(1,1), Vector2(1,0), Vector2(1,-1), Vector2(0,-1)]

func live():
	state = 1
	next = state
	%Color.modulate = "ff27ff"

func die():
	state = 0
	next = state
	%Color.modulate = "64993e"

func reset():
	state = 0
	next = state
	now_count = 0
	next_count = now_count
	%Color.modulate = "64993e"

func _update_neighbors(change, pos, now=false):
	for coords in NEIGHBORS:
		var check = strategy.edge(pos, coords)
		if check != Vector2.ZERO:
			get_parent().grid[check.x][check.y].next_count += change
			if now:
				get_parent().grid[check.x][check.y].now_count += change

func calculate_generation():
	if state == 0:
		if strategy.death_to_life(now_count):
			next = 1
			_update_neighbors(1, pos)
	else: # state = 1
		if strategy.life_to_death(now_count):
			next = 0
			_update_neighbors(-1, pos)
	
func tick_generation():
	now_count = next_count
	if state == next:
		return
	elif next == 1:
		live()
	elif next == 0:
		die()
	else:
		print("IMPOSSIBLE")
	
func new(x, y, strategy):
	pos = Vector2(x,y)
	self.strategy = strategy
	position = Vector2(x_offset + x*(size+padding), y_offset + y*(size+padding))
	return self

func _toggle(viewport, event, shape_idx):
	if strategy.toggle(event):
		if state == 0:
			_update_neighbors(1, pos, true)
			live()
		elif state == 1:
			_update_neighbors(-1, pos, true)
			die()
